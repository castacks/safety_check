#ifndef SAFETYCHECKBASE_H_
#define SAFETYCHECKBASE_H_
#include "guaranteedSafePlanning/abortPathLibraryBase.h"
#include "nav_msgs/Odometry.h"
#include "ca_common/Trajectory.h"
#include "visualization_msgs/MarkerArray.h"
#include <string>
#include <vector>

namespace CA
{
  class safetyCheckBase
  {
    public:
    abortPathLibraryBase* abortLibrary;
    int paramConsiderOccupied;
    int paramConsiderUnknown;
    double paramVResolution;
    double paramUnknownFractionAllowed;

    void setVelocityResolution(double param){paramVResolution = param;};
    void setConsiderOccupied(int considerOccupied){paramConsiderOccupied = considerOccupied;};
    void setConsiderUnknown(int considerUnknown){paramConsiderUnknown = considerUnknown;};
    void setUnknownFractionAllowed(double param){paramUnknownFractionAllowed = param;};
    void setAbortPathLibrary(abortPathLibraryBase* abLib){ abortLibrary = abLib;};
    virtual std::vector<ca_common::Trajectory> returnSafeTrajectories(nav_msgs::Odometry &qState);
    virtual std::vector<ca_common::Trajectory> returnSafeTrajectories(std::vector<ca_common::Trajectory> &trajectoryVector,
                                                                        double numTrajectories = 0)=0;
    virtual std::vector<ca_common::Trajectory> returnSafeVelocity(nav_msgs::Odometry &qState,double &velocity);
    virtual void generateMarkersSafeUnSafeTrajectories(visualization_msgs::MarkerArray &markerArray,
                                                       std::vector<ca_common::Trajectory> *safeTrajectoryVector,
                                                       std::vector<ca_common::Trajectory> *allTrajectoryVector = NULL);
    virtual BBox getUnsafeBoundingBox(nav_msgs::Odometry &qState, bool &valid);
    virtual ~safetyCheckBase(){};
  };
};
#endif
