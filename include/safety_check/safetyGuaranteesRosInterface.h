#ifndef SAFETYGUARANTEESROSINTERFACE_H_
#define SAFETYGUARANTEESROSINTERFACE_H_

#include "guaranteedSafePlanning/safetyCheckBase.h"
#include <string>
#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "std_msgs/Float64.h"

namespace CA
{
  class safetyGuaranteesROSInterface
  {
    public:
    safetyCheckBase* safetycheck;
    nav_msgs::Odometry qState;
    bool framesConsistent;
    ros::Subscriber stateSub;
    ros::Publisher markerArrayPub;
    ros::Publisher safeVelocityPub;
    void checkFrameConsistency(std::string stateFrame, std::string gridFrame);
    void stateCallback(const nav_msgs::Odometry::ConstPtr& msg);
    void process();
    void initialize(ros::NodeHandle &n, safetyCheckBase* safetycheck_);
  };
}


#endif
