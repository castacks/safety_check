#ifndef ACSSAFETYCHECK_H_
#define ACSSAFETYCHECK_H_
#include "guaranteedSafePlanning/hybridAbortPathLibraryHelicopter.h"
#include "guaranteedSafePlanning/safetyCheckBase.h"
#include "perception_interface/grid_interface.h"
#include "acs_common/VehicleDynamicsConstraints.h"

namespace CA
{
  class acsSafetyCheck: public safetyCheckBase
  {
    hybridAbortPathLibrary hybridAbortPathLib;
    public:
    NEA::GridInterface *gridInterface;
    bool isGridExternal;
    bool initialize(ros::NodeHandle &n ,NEA::GridInterface *gInterface = NULL);
    void initializeGridInterface(ros::NodeHandle &n);
    void setGridInterface(NEA::GridInterface *gInterface);
    std::vector<ca_common::Trajectory> returnSafeTrajectories(std::vector<ca_common::Trajectory> &trajectoryVector,
                                                                double numTrajectories = 0);

    void setVehicleDynamicsConstraints(const acs_common::VehicleDynamicsConstraints& msg);
    acsSafetyCheck(){isGridExternal = false;};
    ~acsSafetyCheck()
    {
      if(!isGridExternal)
        delete gridInterface;
    };
  };
}
#endif /* ACSSAFETYCHECK_H_ */
